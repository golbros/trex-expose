// module.exports = {
//   plugins: ["prettier", "react-app", "jsx-a11y"],
//   extends: ["react-app", "plugin:jsx-a11y/recommended"],
//   rules: {
//     "import/no-anonymous-default-export": "error",
//     "import/no-webpack-loader-syntax": "off",
//     "react/react-in-jsx-scope": "off", // React is always in scope with Blitz
//     "jsx-a11y/anchor-is-valid": "off", //Doesn't play well with Blitz/Next <Link> usage
//   },
// }

module.exports = {
  plugins: ["prettier", "react", "jsx-a11y"],
  extends: [
    "react-app",
    // 'plugin:jsx-a11y/recommended',
    "eslint:recommended",
    "plugin:react/recommended",
    "plugin:prettier/recommended",
  ],
  rules: {
    "prettier/prettier": "error",
    "import/no-anonymous-default-export": "off",
    "import/no-webpack-loader-syntax": "off",
    "react/react-in-jsx-scope": "off", // React is always in scope with Blitz
    "jsx-a11y/anchor-is-valid": "off", //Doesn't play well with Blitz/Next <Link> usage

    "react/display-name": "off",
    "react/prop-types": "off",
  },
}
