import { useEffect, useRef, useState, useLayoutEffect, Fragment } from "react"
import classnames from "classnames"
import { debounce } from "lodash"
import mapboxgl from "mapbox-gl"
import Scrollspy from "react-scrollspy"
import dynamic from "next/dynamic"
// @ts-ignore
const Widget = dynamic(() => import("react-chat-widget").then((mod) => mod.Widget), {
  ssr: false,
})

mapboxgl.accessToken =
  "pk.eyJ1Ijoic2ViYXN0aWFuaG9pdHoiLCJhIjoiY2tlMms2eTJyMGEzcDJzbzR2cXF2ZGJ3MSJ9.psZbzav7ua3gHfWZliLD1w"

const handleNewUserMessage = (newMessage) => {
  console.log(`New message incoming! ${newMessage}`)
  // Now send the message throught the backend API
}

const SideMenu = () => {
  const [widthPercentage, setWidthPercentage] = useState(0)
  const [showMenu, setShowMenu] = useState(false)
  const scrollingEvent = () => {
    setShowMenu(window.scrollY > 80)
    setWidthPercentage((window.scrollY + window.outerHeight) / (document.body.clientHeight / 100))
  }

  useEffect(() => {
    window.addEventListener("scroll", scrollingEvent)
  })

  return (
    <div
      style={{ top: "0", opacity: showMenu ? "1" : "0" }}
      className="backdrop-filter fixed font-bold bg-white border-l border-t border-b border-gray-500 right-0 left-0 z-30 rounded-bl rounded-br w-auto"
    >
      <div className="flex flex-col">
        <div
          style={{ width: `${widthPercentage}%`, height: "5px" }}
          className="bg-blue-600 rounded-r"
        />
        <div className="py-2 px-4 overflow-auto">
          <Scrollspy
            items={[
              "start",
              "living",
              "bedroom",
              "kitchengarden",
              "bath",
              "lage",
              "calendar",
              "table",
            ]}
            className="flex flex-row px-4"
            currentClassName="font-thin"
          >
            <li className="py-2 mr-3">
              <a href="#start">Garten</a>
            </li>
            <li className="py-2 mr-3">
              <a href="#living">Wohnzimmer</a>
            </li>
            <li className="py-2 mr-3">
              <a href="#bedroom">Schlafzimmer</a>
            </li>
            <li className="py-2 mr-3">
              <a href="#kitchengarden">Küche</a>
            </li>
            <li className="py-2 mr-3">
              <a href="#bath">Bad</a>
            </li>
            <li className="py-2 mr-3">
              <a href="#lage">Lage</a>
            </li>
            <li className="py-2 mr-3">
              <a href="#calendar">Termin</a>
            </li>
            <li className="py-2 mr-3">
              <a href="#table">Esszimmer</a>
            </li>
          </Scrollspy>
        </div>
      </div>
    </div>
  )
}

const MapSlide = () => {
  const mapContainer = useRef(null)
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: "mapbox://styles/mapbox/light-v9",
      center: [9.8867, 53.5536],
      zoom: 13.01,
    })
  })
  return (
    <div className="w-full flex flex-row flex-wrap">
      <div className="lg:min-h-screen w-full md:w-1/2" ref={mapContainer} />
      <div className="lg:min-h-screen w-full md:w-1/2 p-4">
        <h2 className="text-2xl">Point of Interests</h2>
        <div className="flex flex-col w-full py-10">
          <h3>Alltag</h3>
          <div className=" flex flex-row">
            <div className="w-48 cursor-pointer hover:bg-gray-50 py-5 mr-2 border rounded flex flex-col items-center">
              <svg
                style={{ width: "30px" }}
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M14.121 15.536c-1.171 1.952-3.07 1.952-4.242 0-1.172-1.953-1.172-5.119 0-7.072 1.171-1.952 3.07-1.952 4.242 0M8 10.5h4m-4 3h4m9-1.5a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
              <span>Bankautomaten</span>
            </div>
            <div className="w-48 cursor-pointer hover:bg-gray-50 py-5 border rounded flex flex-col items-center">
              <svg
                style={{ width: "30px" }}
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  stroke-width="2"
                  d="M3 3h2l.4 2M7 13h10l4-8H5.4M7 13L5.4 5M7 13l-2.293 2.293c-.63.63-.184 1.707.707 1.707H17m0 0a2 2 0 100 4 2 2 0 000-4zm-8 2a2 2 0 11-4 0 2 2 0 014 0z"
                />
              </svg>

              <span>Supermärkte</span>
            </div>
          </div>
          <h3 className="mt-5">Freizeit</h3>
          <div className=" flex flex-row">
            <div className="w-48 cursor-pointer hover:bg-gray-50 py-5 mr-2 border rounded flex flex-col items-center">
              <svg
                style={{ width: "30px" }}
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M9 19V6l12-3v13M9 19c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zm12-3c0 1.105-1.343 2-3 2s-3-.895-3-2 1.343-2 3-2 3 .895 3 2zM9 10l12-3"
                />
              </svg>
              <span>Bars</span>
            </div>
            <div className="w-48 cursor-pointer hover:bg-gray-50 py-5 border rounded flex flex-col items-center">
              <svg
                style={{ width: "30px" }}
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z"
                />
              </svg>

              <span>Clubs</span>
            </div>
          </div>
        </div>
      </div>
      {/* <img
          src="https://api.mapbox.com/styles/v1/mapbox/light-v9/static/9.8867,53.5536,13.01,0.00,0.00/1000x600@2x?access_token=pk.eyJ1Ijoic2ViYXN0aWFuaG9pdHoiLCJhIjoiY2tlMms2eTJyMGEzcDJzbzR2cXF2ZGJ3MSJ9.psZbzav7ua3gHfWZliLD1w"
          alt="Karte"
          className="object-fill inline-block h-full"
        /> */}
    </div>
  )
}
const LageSlide = () => {
  // return (
  //   <div className="min-h-screen flex flex-row flex-no-wrap">
  //     <div className="bg-red-500 w-1/2 min-h-screen">asdasd</div>
  //     <div className="text-xl w-1/2 h-full">Lage</div>
  //   </div>
  // )

  return (
    <div
      id="lage"
      className="w-full border-t bg-white md:px-20 md:py-20 text-gray-900 flex flex-col md:flex-row"
    >
      {/* <div className="md:w-3/4 flex mx-auto flex-col md:flex-row md:px-0"> */}
      <div className="w-full md:w-full text-gray-900 px-10 md:px-0 self-center">
        <div className="pt-12 md:pr-20">
          <h2 className="text-5xl font-semibold my-4">Lage</h2>
          <p className="mb-4">
            Die angebotene Wohnung befindet sich im zentralen und beliebten Stadtteil Barmbek-Süd.
            Dieser liegt im Nordosten Hamburgs und grenzt u.a. an die Stadtteile Winterhude und
            Uhlenhorst. Barmbek-Süd zeichnet sich architektonisch durch eine Mischung aus
            Jugendstilaltbauten und Rotklinker sowie durch die direkte Nähe zur Hamburger Innenstadt
            aus.
          </p>
          <p className="mb-4">
            Die Wohnung liegt zentral und dennoch ruhig in einer Wohnstraße. Ein abwechslungsreiches
            Freizeitangebot bietet sowohl der nahe gelegene Stadtpark als auch die Außenalster mit
            ihren Parkanlagen.
          </p>
          <p className="mb-4">
            Zur kulturellen Unterhaltung laden das nahe gelegene ’Ernst-Deutsch-Theater’, ’The
            English Theatre of Hamburg’ sowie das ’UCI Mundsburg’ ein. In ca. 20 Minuten erreichen
            Sie die Hamburger Innenstadt mit dem Fahrrad, um dort weitere kulturelle Highlights
            genießen zu können.
          </p>
          <p className="mb-4">
            Eine optimale Anbindung an öffentliche Verkehrsmittel wird durch die U-Bahnstation
            ’Dehnhaide’ und ”Hamburger Straße” gewährleistet, welche ebenfalls fußläufig zu
            erreichen sind. Weitere Einkaufsmöglichkeiten befinden sich in unmittelbarer Nähe,
            beispielsweise im Einkaufszentrum ’Hamburger Meile’.
          </p>
        </div>
      </div>
      {/* </div> */}
    </div>
  )
}

const ContentDienstleistung = () => {
  return (
    <div className="min-h-screen py-20 bg-gray-100 text-gray-900 flex flex-wrap items-center text-xl">
      <div className="px-4 md:px-0 md:w-1/2 m-auto">
        <h2 className="text-5xl font-semibold mb-8 text-center">Dienstleistung</h2>
        <div className="flex flex-wrap">
          <div className="w-full md:w-1/2 mb-4 md:mb-0">
            <h3 className="text-xl mb-2 font-bold">Standard</h3>
            <ul>
              <li>Dreharbeiten mit 1x Kameramann</li>
              <li>- Außenschuss 2x</li>
              <li>- Wohnzimmmer 2x</li>
              <li>- Schlafzimmer 3x</li>
              <li>- Küche 1x</li>
              <li>- Bad 1x</li>
              <li>Aufbereitung für die Einbindung in die Website</li>
              <li>Farbkorrektur</li>
              <li>Simple Retusche</li>
            </ul>
          </div>
          <div className="w-full md:w-1/2 md:pl-8">
            <h3 className="text-xl mb-2 font-bold">Extra-Optionen</h3>
            <ul>
              <li>Weitere Zimmer bei größeren Immobilien</li>
              <li>Einsatz einer 360 Grad Kamera</li>
              <li>Export einzelner Fotos in 35 MP</li>
              <li>Export einzelner Videos in 4K</li>
              <li>Aufbereitung zu Social Media Material</li>
              <li>Wöchentlich</li>
              <li>Monatlich</li>
              <li>Jährlich</li>
              <li>Zeitraffer</li>
              <li>Tag und Nachtaufnahmen</li>
              <li>Drohnen-Aufnahmen</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

const ContentSoftware = () => {
  return (
    <div className="min-h-screen py-20 sm:py-0 bg-gray-100 text-gray-900 flex flex-wrap items-center text-xl">
      <div className="w-full px-4 md:px-0 md:w-1/2 m-auto">
        <h2 className="text-5xl font-semibold mt-20 md:mt-0 mb-8 text-center">Software Features</h2>
        <div className="flex flex-wrap">
          <div className="w-full md:w-1/2 mb-4 md:mb-0">
            <h3 className="text-xl mb-2 font-bold">Standard</h3>
            <ul>
              <li>Landingpage zum selber Texte einfügen</li>
              <li>Angepasst auf Desktop und Mobil</li>
              <li>Reporting von Nutzerdaten</li>
              <li>Reporting zum Nutzerverhalten</li>
              <li>Karte / Stadtplan </li>
              <li>Website als Printversion</li>
            </ul>
          </div>
          <div className="w-full md:w-1/2 md:pl-8">
            <h3 className="text-xl mb-2 font-bold">Extra-Optionen</h3>
            <ul>
              <li>Anpassung an Unternehmens-CI</li>
              <li>Login & Registrierung</li>
              <li>Interaktiver Stadtplan Verknüpfter Kalender für Besichtigungen</li>
              <li>Chat-Funktion für Rückfragen</li>
              <li>Einbindung in eigene App</li>
              <li>Eigene Features mitentwickeln</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  )
}

const ContentPricing = () => {
  const pricingBlocks = [
    <>
      <h3 className="font-bold">Tester</h3>
      <p>
        Bis 8 Einheiten
        <br />
        Innerhalb von 3 Monaten á 1.500 €
      </p>
    </>,

    <>
      <h3 className="font-bold">Starter</h3>
      <p>
        Ab 8 Einheiten
        <br />
        Innerhalb von 3 Monaten á 1.300 €
      </p>
    </>,

    <>
      <h3 className="font-bold">Small</h3>
      <p>
        Ab 15 Einheiten
        <br />
        Innerhalb von 6 Monaten á 1.000 €
      </p>
    </>,

    <>
      <h3 className="font-bold">Medium</h3>
      <p>
        60 Einheiten
        <br />
        Innerhalb von 6 Monaten á 800 €
      </p>
    </>,

    <>
      <h3 className="font-bold">Large</h3>
      <p>
        120 Einheiten
        <br />
        Innerhalb von 6 Monaten á 600 €
      </p>
    </>,

    <>
      <h3 className="font-bold">Make it your own</h3>
      <p>
        240 Einheiten
        <br />
        Innerhalb eines Jahres á 400 €
      </p>
    </>,
  ]
  return (
    <div className="min-h-screen bg-gray-100 text-gray-900 flex flex-wrap items-center text-xl">
      <div className="px-4 md:px-0 md:w-2/3 m-auto">
        <h2 className="text-5xl font-semibold mb-8 text-center">Preise</h2>
        <div className="flex flex-wrap">
          {pricingBlocks.map((elm, idx) => (
            <div className="w-1/2 md:w-1/3 md:px-8 py-4" key={idx}>
              {elm}
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}

const ContentUsp = () => {
  return (
    <div className="h-screen text-gray-100 bg-gray-900 flex flex-wrap items-center text-xl">
      <div className="px-4 md:px-0 md:w-2/3 m-auto">
        <p className="text-center text-2xl font-bold md:font-normal md:text-5xl break-words">
          Sie stechen aus dem Markt heraus.
          <br />
          Sie stärken Ihre Marke und erzeugen einen hohen Wiedererkennungswert.
        </p>
      </div>
    </div>
  )
}

const FaktenSlide = () => {
  const faktenMap = {
    Wohnfläche: "ca. 77qm",
    "Anzahl Zimmer": 4,
    Baujahr: "1927 / 2015 saniert",
    Kaufpreis: "EUR 435.000,-",
    Käuferprovision: "provisionsfrei",
    "Hausgeld pro Monat": "EUR 395,-",
    Heizungsart: "Fernwärme",
    Energieausweis:
      "Energieausweis: verbrauchsorientiert // Endenergieverbrauch: 150 kWh/(qm * a) // Energieeffizienzklasse",
    Übergabe: "erfolgt nach Vereinbarung",
  }

  return (
    <div className="min-h-screen bg-gray-100 text-gray-900 flex flex-wrap items-center text-xl">
      <div className="md:w-1/2 m-auto  p-3 md:p-10 md:shadow-2xl md:border md:border-gray-200">
        <h2 className="text-5xl font-semibold my-4">Fakten</h2>
        <dl className="flex flex-wrap">
          {Object.keys(faktenMap).map((key, idx) => (
            <Fragment key={idx}>
              <dt className="w-1/2 md:w-1/3 text-left py-1 pr-10">{key}</dt>
              <dd className="w-1/2 md:w-2/3 py-1 break-words">{faktenMap[key]}</dd>
            </Fragment>
          ))}
        </dl>
      </div>
    </div>
  )
}

const CalendarSlide = () => {
  useEffect(() => {
    const head = document.querySelector("head")
    const script = document.createElement("script")
    script.setAttribute("src", "https://assets.calendly.com/assets/external/widget.js")
    if (head) {
      head.appendChild(script)
    }
  })
  return (
    <div
      id="calendar"
      className="min-h-screen py-20 md:py-3 bg-white text-gray-900 flex flex-col flex-wrap items-center text-xl"
    >
      <h2 className="text-5xl text-center mb-8 break-all">Besichtigungstermin buchen</h2>
      <div
        className="calendly-inline-widget md:w-1/2 mx-auto h-screen"
        data-url="https://calendly.com/trex-expose/besichtigung"
        style={{ height: "950px" }}
      ></div>
    </div>
  )
}

const FrameSlide = (props) => {
  const id = props.id

  const desc = props.desc || { title: "Title", sub: "Details zum Raum" }
  const frames = props.frames
  const playbackConstant = 50
  const height = frames * playbackConstant
  const containerStretchRef = useRef<HTMLDivElement>(null)
  const container = useRef<HTMLDivElement>(null)
  const [activeFrame, setActiveFrame] = useState(0)
  const [isScrolling, setIsScrolling] = useState(false)
  const [isActive, setIsActive] = useState(false)

  const debouncedScrollReset = debounce(() => {
    setIsScrolling(false)
  }, 100)

  const arr = Array.from(Array(frames).keys())

  const scrollPlay = () => {
    setIsScrolling(true)
    debouncedScrollReset()

    if (!containerStretchRef.current) return
    const boundingRect = containerStretchRef.current.getBoundingClientRect()

    let active = boundingRect.top <= 0 && boundingRect.bottom >= window.innerHeight
    if (active) {
      const scrollPercent = Math.abs(boundingRect.top) / (boundingRect.height - window.innerHeight)
      const frameNumber = Math.floor(scrollPercent * frames)

      requestAnimationFrame(() => {
        if (!container.current) return
        setActiveFrame(Math.min(frameNumber, frames - 1))
        setIsActive(true)
        // container.current.children[frameNumber].
      })
    }
    setIsActive(false)
    // requestAnimationFrame(scrollPlay)
  }

  useEffect(() => {
    // ref.current = requestAnimationFrame(scrollPlay)
    window.addEventListener("scroll", scrollPlay)

    return () => {
      window.removeEventListener("scroll", scrollPlay)
      // cancelAnimationFrame(ref.current)
    }
  }, [])

  return (
    <div
      id={id}
      className="bg-gray-900 relative"
      ref={containerStretchRef}
      style={height > 0 ? { height: height + "px" } : {}}
    >
      <div className="sticky top-0 h-screen" ref={container}>
        {arr.map((e, idx) => {
          return (
            <div
              key={idx}
              className={classnames("h-screen absolute inset-0 flex align-middle", {
                hidden: activeFrame !== e,
              })}
            >
              <img
                src={props.path
                  .replace(
                    "{res}",
                    /*
                      show large image IF:
                        - the current Frame is activated (scrubbed to), the user is not scrolling and the current video is in view
                        - OR it is the first or last frame
                    */
                    (activeFrame === e && !isScrolling && isActive) ||
                      idx === 0 ||
                      idx === arr.length - 1
                      ? "large"
                      : "small"
                  )
                  // .replace("{res}", "small")
                  .replace("{x}", e + 1)}
                className="w-full object-cover"
                alt={`Slide ${e}`}
              />
            </div>
          )
        })}
        {props.cover && <props.cover classNames={classnames({ "opacity-0": activeFrame > 3 })} />}
        {props.outro && (
          <props.outro classNames={classnames({ "opacity-0": activeFrame < frames - 3 })} />
        )}
        <div
          className="absolute bg-white mr-5 border rounded mt-5 backdrop-filter"
          style={{
            left: "30px",
            top: 70 /* + (height / frames) * 0.02 * activeFrame */,
          }}
        >
          <h2 className="text-3xl font-bold px-5 font-thin">{desc.title}</h2>
          <h3 style={{ letterSpacing: "0px" }} className="mt-2 px-5 pb-2">
            {desc.sub}
          </h3>
        </div>
      </div>
    </div>
  )
}

const VideoSlide = (props) => {
  const ref = useRef<number>()
  const containerStretchRef = useRef<HTMLDivElement>(null)
  const videoRef = useRef<HTMLVideoElement>(null)
  const playbackConstant = 1000
  const [height, setHeight] = useState(0)

  const scrollPlay = () => {
    if (!videoRef.current || !containerStretchRef.current) return
    const boundingRect = containerStretchRef.current.getBoundingClientRect()

    let active = boundingRect.top <= 0 && boundingRect.bottom >= window.innerHeight
    if (active) {
      const scrollPercent = Math.abs(boundingRect.top) / (boundingRect.height - window.innerHeight)
      const frameNumber = scrollPercent * videoRef.current.duration
      videoRef.current.currentTime = frameNumber
      requestAnimationFrame(() => {
        if (!videoRef.current) return
        videoRef.current.currentTime = frameNumber
      })
    }
    // requestAnimationFrame(scrollPlay)
  }

  // useLayoutEffect(() => {

  // })

  useLayoutEffect(() => {
    // ref.current = requestAnimationFrame(scrollPlay)
    window.addEventListener("scroll", scrollPlay)

    return () => {
      window.removeEventListener("scroll", scrollPlay)
      if (!ref.current) return
      // cancelAnimationFrame(ref.current)
    }
  }, [])

  useEffect(() => {
    if (videoRef.current?.duration) {
      onLoadedMetadata()
    }
  })

  const onLoadedMetadata = () => {
    if (!videoRef.current) {
      return
    }
    setHeight(Math.floor(videoRef.current.duration * playbackConstant))
    // containerStretchRef.current?.style.height = +"px"
  }

  return (
    <div
      className="bg-gray-900"
      style={height > 0 ? { height: height + "px" } : {}}
      ref={containerStretchRef}
    >
      <video
        preload="auto"
        ref={videoRef}
        onLoadedMetadata={onLoadedMetadata}
        className={classnames("sticky top-0 w-full h-screen", {
          // "fixed left-0 right-0 bottom-0 top-0": state === "active",
          // "absolute bottom-0 left-0 right-0": state === "bottom",
        })}
      >
        <source type="video/webm" src={`/videos/${props.video}`} />
      </video>
    </div>
  )
}
const Home = () => {
  const videos = [
    // "foreign.mp4",
    // "bedroom_kfd_1.mp4",
    // "webm/A003_C001_0813G7_001_3_1_1.webm",
    // "house_opt3.mp4",
    // "house_opt.mp4",
    // "house_1.mp4",
    // "house_2.mp4",

    "opt/house.mp4",
    "opt/house_1.mp4",
    "opt/house_2.mp4",
    "opt/livingroom.mp4",
    "opt/livingroom_detail_1.mp4",
    "opt/livingroom_detail_2.mp4",
    "opt/livingroom_detail_3.mp4",
  ]

  const frameSlides = [
    {
      id: "start",
      path: "/videos/frames/garden_{res}/garden_{x}.jpg",
      desc: {
        title: "Sonniger Garten",
        sub: "120qm | Schichtschutz | Garten",
      },
      frames: 100,
      cover: (props) => (
        <div>
          <div
            className={classnames(
              "absolute inset-0 z-10 flex flex-wrap content-center justify-center transition duration-500 ease-in-out",
              props.classNames
            )}
          >
            <div className="z-20 md:w-1/2">
              <div className="md:bg-white rounded p-6 flex mx-auto flex-col md:flex-row shadow-2xl">
                <div className="flex items-center w-4 mx-auto w-full md:p-10 py-20 border-gray-900 md:border-r-2 text-center md:text-center">
                  <img
                    src="https://placeholder.com/wp-content/uploads/2018/10/placeholder.com-logo3.png"
                    alt="Logo"
                    className="object-cover rounded-lg inline-block"
                  />
                </div>
                <div className="w-full text-gray-900 px-4 md:p-10 self-center">
                  <h1 className="text-2xl font-light text-gray-900 md:mb-4">Registrieren</h1>

                  <form className="grid grid-cols-1 gap-4">
                    <div className="grid grid-cols-2 gap-2">
                      <input
                        className="shadow appearance-none w-full border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        type="text"
                        placeholder="Name"
                      />
                      <input
                        className="shadow appearance-none w-full border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                        type="text"
                        placeholder="Vorname"
                      />
                    </div>
                    <input
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="text"
                      placeholder="E-Mail"
                    />
                    <input
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="email"
                      placeholder="Phone"
                    />
                    <input
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="email"
                      placeholder="Alter"
                    />
                    <input
                      className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                      type="email"
                      placeholder="PLZ"
                    />
                  </form>
                </div>
              </div>
            </div>
            <div className="absolute inset-0 opacity-100 z-10 backdrop-filter" />
          </div>
        </div>
      ),
    },
  ]

  const indoorSlides = [
    {
      path: "/videos/frames/living_{res}/living_{x}.jpg",
      frames: 66,
      id: "living",
      desc: {
        title: "Wohnzimmer",
        sub: "Großes und gemütliches Wohnzimmer. Parkett. 25qm",
      },
    },
    // { path: "/videos/frames/outside/outside_{x}.jpg", frames: 96 },
    {
      path: "/videos/frames/room_{res}/bedroom_{x}.jpg",
      frames: 100,
      id: "bedroom",
      desc: {
        title: "Schlafzimmer",
        sub: "20qm, Zugang direkt zum Balkon",
      },
    },
  ]

  const additionalSlides = [
    {
      path: "/videos/frames/kitchengarden_{res}/kitchengarden_{x}.jpg",
      frames: 100,
      id: "kitchengarden",
      desc: {
        title: "Küche",
        sub: "15qm, Dunstabzugshaube, 2 Arbeitsflächen",
      },
    },
    { path: "/videos/frames/bath_{res}/bath_{x}.jpg", frames: 100, id: "bath" },
  ]

  const lastSlides = [
    {
      path: "/videos/frames/table_{res}/table_{x}.jpg",
      id: "table",
      frames: 100,
      outro: (props) => {
        const contact = {
          Telefon: "+49 175 2517038",
          "E-Mail": "hi@makemake.sh",
          Adresse: "Am Sandtorkai 27, 20457 Hamburg",
        }
        return (
          <>
            <div
              className={classnames(
                "absolute inset-0 z-10 flex flex-wrap content-center justify-center transition duration-500 ease-in-out",
                props.classNames
              )}
            >
              <div className="z-20 md:w-1/2">
                <div className="flex mx-auto flex-row md:bg-white rounded p-6 flex mx-auto flex-col md:flex-row shadow-2xl">
                  <div className="w-3/4 mx-auto md:w-full px-4 md:px-0 md:pr-20 py-20 md:mr-20 border-gray-900 md:border-r-2 text-center md:text-right">
                    <img
                      src="https://images.unsplash.com/photo-1519085360753-af0119f7cbe7?ixlib=rb-1.2.1&fit=crop&w=450&h=600&q=80&crop=faces"
                      alt="Makler"
                      className="object-cover rounded-lg inline-block"
                    />
                  </div>
                  <div className="w-full text-gray-900 px-4 md:px-0 self-center">
                    <h2 className="text-5xl font-semibold md:text-4xl mb-4">Kontakt</h2>
                    <h3 className="text-2xl mb-4">Sebastian H.</h3>
                    <dl className="flex flex-wrap">
                      {Object.keys(contact).map((key, idx) => (
                        <Fragment key={idx}>
                          <dt className="w-1/2 bg-blue-100 rounded-l lg:w-1/2 mb-1 px-2 py-3 pr-10">
                            {key}
                          </dt>
                          <dd className="w-1/2 bg-blue-200 lg:w-1/2 rounded-r mb-1 px-2 py-3 break-words">
                            {contact[key]}
                          </dd>
                        </Fragment>
                      ))}
                    </dl>
                  </div>
                </div>
              </div>
              <div className="absolute inset-0 bg-blue-300 opacity-75 z-10 backdrop-filter" />
            </div>
          </>
        )
      },
    },
  ]

  return (
    <div style={{ scrollSnapType: "y mandatory" }}>
      <SideMenu />
      <Widget
        title="T.REX Chat System"
        subtitle="Kontaktieren Sie uns"
        handleNewUserMessage={handleNewUserMessage}
      />
      {/* {videos.map((e, idx) => (
        <VideoSlide key={idx} video={e} />
      ))} */}
      {/*
      <div className="fixed top-0 right-0 z-20">
        <div
          className="w-16 h-16 flex flex-wrap justify-center items-center align-middle text-gray-700 bg-white rounded-full m-4 text-center font-mono text-xl font-bold cursor-pointer"
          onClick={() => {
            window.scrollTo({
              top: 0,
              behavior: "smooth",
            })
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="stroke-current w-6 h-6"
          >
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M5 15l7-7 7 7" />
          </svg>
        </div>
      </div>
      */}
      {frameSlides.map((e, idx) => (
        <FrameSlide key={idx} {...e} />
      ))}
      {/* <ContentDienstleistung />
      <ContentSoftware /> */}
      {indoorSlides.map((e, idx) => (
        <FrameSlide key={idx} {...e} />
      ))}
      {/* <ContentPricing /> */}
      {/* <ContentUsp /> */}
      {additionalSlides.map((e, idx) => (
        <FrameSlide key={idx} {...e} />
      ))}
      <MapSlide />
      <LageSlide />
      <FaktenSlide />
      <CalendarSlide />
      {lastSlides.map((e, idx) => (
        <FrameSlide key={idx} {...e} />
      ))}
    </div>
  )
}

export default Home
